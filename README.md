This is a readme file. This is for the project of creating a scanner using jflex.
The scanner of which will return tokens for identifiers, numbers and all the keywords and symbols which is provided in a list.
Make sure to have a jdk on your computer or laptop before doing the steps below.

How to run the scanner once cloned
----------------------------------
1)Open the terminal(search for it unless already on desktop or taskbar)
2)cd directory(wherever you cloned the project to. E.g. If cloned to desktop
then put on the command line cd Desktop)
3)cd lorjflex
4)java -jar jflex.jar flexscanner.flex
5)javac Main.java
6)java Main .txt file (there will be 2 .txt file. The 1st .txt file is called
some.txt where it as many things to test the scanner. The 2nd .txt file is called
some-error.txt where it will have some errors for the scanner to catch.)