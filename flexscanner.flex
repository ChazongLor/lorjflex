/*
 * Create a scanner which returns tokens for identifiers, numbers, and all the keywords
 * and symbols. The scanner may either ignore comments, treating them as white space, or return
 * them a COMMENT token.
 *
 */
 
  /* User Comments and Code */
 
 
 
 
 
 
 
 
 
%%
 
 /*   Directives and Macros    */
 /*
  *  %class     ClassName  Name of generated class
  *  %function name        Name of the scanning method
  *  %integer              Causes the return type of scanning method to be int
  *  %type   TypeName      Return type of scanning method will be TypeName
  *  %eofval{              Code to return some value at the end of file, this
  *  %eofval}                must match the return type of scanning method.
  *
  *  %standalone          Creates a main method in generated class
  *
  *  Macros are of the form MacroName = regular expression
  *  To use an already-defined macro in a regular expression, use {MacroName}
  */
  
%class ScannerJFlex
%function nextToken
%type Token
%eofval{
 return null;
%eofval}

 /* Patterns */
other = .
letter = [A-Za-z]
digit = [1-9][0-9]*
number = {digit}+
word = {letter}+
operator = [\*\+\-\/\=]
symbol = [\(\)\{\}\[\]\;\:\!]
compare = [\>\<\>=\<=\!=\|\&]
whitespace = [\t\n\r]
  



%%  
/* Lexical Rules  */
/* The rules for what to do when a pattern is matched.
 * These are of the form {patternname} {code to run on match}
 */

{number} {
  return new Token(yytext(), TokenType.NUMBER);	     
}

{word} {
  return new Token(yytext(), TokenType.ID);	
}

{operator} {
  TokenType tt = null;
  switch(yytext()){
	case "+" : tt = TokenType.PLUS; break;
	case "-" : tt = TokenType.MINUS; break;
	case "*" : tt = TokenType.MULTIPLY; break;
	case "/" : tt = TokenType.DIVISION; break;
	case "=" : tt = TokenType.EQUAL; break;
  }
  return new Token (yytext(), tt);
}

{symbol} {
  TokenType tt = null;
  switch(yytext()){
	case "(" : tt = TokenType.LPAREN; break;
	case ")" : tt = TokenType.RPAREN; break;
	case "{" : tt = TokenType.LCURLBRACK; break;
	case "}" : tt = TokenType.RCURLBRACK; break;
	case "[" : tt = TokenType.LSQBRACKET; break;
	case "]" : tt = TokenType.RSQBRACKET; break;
	case ":" : tt = TokenType.COLON; break;
	case ";" : tt = TokenType.SEMI_COLON; break;
	case "!" : tt = TokenType.EXCLAMATION; break;
  }
  return new Token (yytext(), tt);
}

{compare} {
  TokenType tt = null;
  switch(yytext()){
	case ">" : tt = TokenType.GREATERTHAN; break;
	case "<" : tt = TokenType.LESSTHAN; break;
	case ">=" : tt = TokenType.GTORE; break;
	case "<=" : tt = TokenType.LTORE; break;
	case "!=" : tt = TokenType.NOTEQUAL; break;
	case "|" : tt = TokenType.OR; break;
	case "&" : tt = TokenType.AND; break;
  }
  return new Token (yytext(), tt);
}

{whitespace} {
  //Do nothing
}

{other} {
  //ignore other stuff
}