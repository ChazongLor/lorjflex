/*
 * Token.java
 */
 
/**
 * Represents a Token with a TokenType and lexeme.
 */
public class Token {

    private String lexeme;
    private TokenType type;
    
    public Token( String lex, TokenType t) {
        this.lexeme = lex;
        this.type = t;
    }
    
    @Override
    public String toString() {
        return this.type + ": " + this.lexeme;
    }
}